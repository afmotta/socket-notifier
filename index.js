const _ = require('lodash');
const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
var redis = require("redis");
var sub = redis.createClient();
var pub = redis.createClient();

const clients = [];

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

app.post('/send', (req, res) => {
  console.log('send');
  const dataJson = JSON.stringify({ userId: 1, data: {"test": "prova"}});
  pub.publish("notifications", dataJson);
  res.sendStatus(200);
});

io.on('connection', (socket) => {
  console.log('a user connected');
  
  socket.on('userId', (userId) => {
    clients.push({ socketId: socket.id, userId: userId });
    console.log('a user login');
    console.log(clients);
  });

  socket.on('disconnect', () => {
    _.remove(clients, (n) => {
      return n.socketId == socket.id;
    });
    console.log('a user disconnect');
    console.log(clients);
  });

});

sub.on("message", function (channel, message) {
    const data = JSON.parse(message);
    console.log("sub channel " + channel + ": " + message);
    if (data && data.userId && data.data) {
      const recipients = _.filter(clients, ['userId', data.userId]);
      _.each(recipients, (recipient) => {
        io.in(recipient.socketId).emit('notification', data.data);
        console.log('sent notification to room', recipient.socketId);
      });
    }
});

sub.subscribe("notifications");

http.listen(3000, () => {
  console.log('listening on *:3000');
});